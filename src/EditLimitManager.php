<?php

namespace Drupal\edit_limit;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\ContentEntityType;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Edit Limit Manager.
 */
class EditLimitManager {

  /**
   * The Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The Edit Limit configurations.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $settings;

  /**
   * Constructs the EditLimitManager object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_ype_manager
   *   The Entity Type Manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   */
  public function __construct(EntityTypeManagerInterface $entity_ype_manager, ConfigFactoryInterface $config_factory) {
    $this->entityTypeManager = $entity_ype_manager;
    $this->settings = $config_factory->get('edit_limit.settings');
  }

  /**
   * Returns array with all content entity types.
   */
  public function getAllContentEntityTypeList() {
    $definitions = $this->entityTypeManager->getDefinitions();
    $content_entity_types = array_filter($definitions, function ($entity_type) {
      return ($entity_type instanceof ContentEntityType);
    });
    $content_entity_types = array_map(function ($entity_type) {
      return $entity_type->getLabel()->__toString();
    }, $content_entity_types);
    return $content_entity_types;
  }

  /**
   * Gets all User's roles list.
   */
  public function getAllRoleList() {
    $roles = $this->entityTypeManager
      ->getStorage('user_role')
      ->loadMultiple();
    $roles = array_map(function ($role) {
      return $role->label();
    }, $roles);

    return $roles;
  }

  /**
   * Returns array with all bundles of entity type.
   *
   * @param string $entity_type_id
   *   The Entity Type ID.
   */
  public function getBundlesListByEntityTypeId(string $entity_type_id) {
    $bundles_list = [];
    $bundles = $this->entityTypeManager
      ->getStorage($this->getBundleEntityTypeId($entity_type_id))
      ->loadMultiple();
    foreach ($bundles as $bundle) {
      $bundles_list[$bundle->id()] = $bundle->label();
    }

    return $bundles_list;
  }

  /**
   * List of checks to prohibit entity editing.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The Entity.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The Current user.
   */
  public function prohibitEditing(EntityInterface $entity, AccountInterface $account) {
    return ($entity instanceof ContentEntityInterface)
      && $this->isUserNotIgnore($account)
      && $this->isEntityTypeTurnOn($entity)
      && $this->isBundleTurnOnIfExist($entity)
      && $this->isEntityLimitExpected($entity);
  }

  /**
   * Checks whether the entity has bundles.
   *
   * @param string $entity_type_id
   *   The Entity Type ID.
   */
  public function hasEntityTypeBundle(string $entity_type_id) {
    $bundle_entity_type_id = $this->getBundleEntityTypeId($entity_type_id);
    return $bundle_entity_type_id != NULL;
  }

  /**
   * Returns the Bundle's Config Entity ID of the Content Entity.
   *
   * @param string $entity_type_id
   *   The Entity Type ID.
   */
  private function getBundleEntityTypeId(string $entity_type_id) {
    return $this->entityTypeManager
      ->getDefinition($entity_type_id)
      ->getBundleEntityType();
  }

  /**
   * Checks whether the bundle is included if the entity contains a bundle.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The Entity.
   */
  private function isBundleTurnOnIfExist(ContentEntityInterface $entity) {
    $settings = $this->settings->get('entity_type');
    $entity_type_id = $entity->getEntityTypeId();

    if ($this->hasEntityTypeBundle($entity_type_id)) {
      return isset($settings[$entity_type_id]['bundles'][$entity->bundle()]);
    }

    return TRUE;
  }

  /**
   * Checks the inclusion of the Entity type.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The Entity.
   */
  private function isEntityTypeTurnOn(ContentEntityInterface $entity) {
    $types = $this->settings->get('entity_types');
    return in_array($entity->getEntityTypeId(), $types, TRUE);
  }

  /**
   * Checks if the limit has expired.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The Entity.
   */
  private function isEntityLimitExpected(EntityInterface $entity) {
    $limit = $this->settings->get('entity_type')[$entity->getEntityTypeId()]['limit'];
    $unit = $this->settings->get('entity_type')[$entity->getEntityTypeId()]['unit'];
    $expected = new \DateTime();
    $expected->modify("-{$limit} {$unit}");
    return $entity->getCreatedTime() <= $expected->getTimestamp();
  }

  /**
   * Checks if user's role isn't ignore.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The currently logged in account.
   */
  private function isUserNotIgnore(AccountInterface $account) {
    $ignore_roles = $this->settings->get('ignore');
    $ignore_roles = array_filter($ignore_roles, function ($role) {
      return $role !== 0;
    });
    $user_roles = $account->getRoles();

    return !array_intersect($user_roles, $ignore_roles);
  }

}
