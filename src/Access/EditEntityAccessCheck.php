<?php

namespace Drupal\edit_limit\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessCheck;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\edit_limit\EditLimitManager;
use Symfony\Component\Routing\Route;

/**
 * Entity Access Check.
 */
class EditEntityAccessCheck extends EntityAccessCheck {

  /**
   * The Edit Limit Helper.
   *
   * @var \Drupal\edit_limit\EditLimitManager
   */
  protected $manager;

  /**
   * Constructs the EditEntityAccessCheck object.
   *
   * @param \Drupal\edit_limit\EditLimitManager $edit_limit_manager
   *   The Edit Limit Helper.
   */
  public function __construct(EditLimitManager $edit_limit_manager) {
    $this->manager = $edit_limit_manager;
  }

  /**
   * {@inheritDoc}
   */
  public function access(Route $route, RouteMatchInterface $route_match, AccountInterface $account) {
    $requirement = $route->getRequirement('_entity_access');
    [$entity_type, $operation] = explode('.', $requirement);
    $entity = $route_match->getParameter($entity_type);

    if ($operation === 'update' && $this->manager->prohibitEditing($entity, $account)) {
      return AccessResult::forbidden();
    }

    return parent::access($route, $route_match, $account);
  }

}
