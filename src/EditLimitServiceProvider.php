<?php

namespace Drupal\edit_limit;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Drupal\edit_limit\Access\EditEntityAccessCheck;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Edit Limit Service Provider.
 */
class EditLimitServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritDoc}
   */
  public function alter(ContainerBuilder $container) {
    $definition = $container->getDefinition('access_check.entity');
    $definition->setClass(EditEntityAccessCheck::class);
    $definition->addArgument(new Reference('edit_limit.manager'));
  }

}
