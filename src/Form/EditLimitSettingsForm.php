<?php

namespace Drupal\edit_limit\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\edit_limit\EditLimitManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Settings form class.
 */
class EditLimitSettingsForm extends ConfigFormBase {

  /**
   * Default value of the Limit field.
   */
  private const DEFAULT_LIMIT_VALUE = 15;

  /**
   * Default unit of the Limit field.
   */
  private const DEFAULT_LIMIT_UNIT = 'minutes';

  /**
   * The Edit Limit Helper.
   *
   * @var \Drupal\edit_limit\EditLimitManager
   */
  protected $editLimitManager;

  /**
   * Constructs the EditLimitSettingsForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface $typedConfigManager
   *   The typed config manager.
   * @param \Drupal\edit_limit\EditLimitManager $edit_limit_manager
   *   EditLimitSettingsForm.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    TypedConfigManagerInterface $typedConfigManager,
    EditLimitManager $edit_limit_manager,
  ) {
    parent::__construct($config_factory, $typedConfigManager);
    $this->editLimitManager = $edit_limit_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('config.typed'),
      $container->get('edit_limit.manager'),
    );
  }

  /**
   * {@inheritDoc}
   */
  protected function getEditableConfigNames() {
    return ['edit_limit.settings'];
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'edit_limit_settings_form';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $settings = $this->config('edit_limit.settings');

    $form['ignore'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Ignore for'),
      '#options' => $this->editLimitManager->getAllRoleList(),
      '#default_value' => $settings->get('ignore') ?: [],
      '#size' => 6,
      '#multiple' => TRUE,
    ];

    $form['entity_types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Entity types'),
      '#options' => $this->editLimitManager->getAllContentEntityTypeList(),
      '#default_value' => $settings->get('entity_types') ?: [],
      '#size' => 6,
      '#multiple' => TRUE,
    ];

    $this->addEntityTypeListForm($form);

    return parent::buildForm($form, $form_state);
  }

  /**
   * Adds form's elements with settings for each Entity Type.
   *
   * @param array $form
   *   Nested array of form elements that comprise the form.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function addEntityTypeListForm(array &$form) {
    $settings = $this->config('edit_limit.settings')->get('entity_type');
    $entity_types = $this->editLimitManager->getAllContentEntityTypeList();
    $form['entity_type'] = [
      '#tree' => TRUE,
    ];

    foreach ($entity_types as $entity_type_id => $name) {
      $form['entity_type'][$entity_type_id] = [
        '#type' => 'details',
        '#title' => $name,
        '#tree' => TRUE,
        '#states' => [
          'visible' => [
            ":input[name='entity_types[$entity_type_id]']" => [
              'checked' => TRUE,
            ],
          ],
        ],
      ];

      $form['entity_type'][$entity_type_id]['unit'] = [
        '#type' => 'select',
        '#title' => $this->t("Limit's unit"),
        '#options' => $this->getLimitUnits(),
        '#default_value' => $settings[$entity_type_id]['unit'] ?? self::DEFAULT_LIMIT_UNIT,
        '#weight' => 0,
      ];

      $form['entity_type'][$entity_type_id]['limit'] = [
        '#type' => 'number',
        '#title' => $this->t('Limit'),
        '#min' => 0,
        '#default_value' => $settings[$entity_type_id]['limit'] ?? self::DEFAULT_LIMIT_VALUE,
        '#weight' => 0,
      ];

      if ($this->editLimitManager->hasEntityTypeBundle($entity_type_id)) {
        $form['entity_type'][$entity_type_id]['bundles'] = [
          '#type' => 'select',
          '#title' => $this->t('Bundles'),
          '#options' => $this->editLimitManager->getBundlesListByEntityTypeId($entity_type_id),
          '#default_value' => $settings[$entity_type_id]['bundles'] ?? [],
          '#size' => 10,
          '#multiple' => TRUE,
        ];
      }
    }
  }

  /**
   * All available units for the Limit field.
   */
  private function getLimitUnits() {
    return [
      'minutes' => $this->t('Minutes'),
      'days' => $this->t('Days'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('edit_limit.settings');

    $config->set('ignore', array_values(array_filter($form_state->getValue('ignore'))));

    $entity_types = array_values(array_filter($form_state->getValue('entity_types')));
    $config->set('entity_types', $entity_types);

    $entity_type = $form_state->getValue('entity_type');
    $entity_type = array_intersect_key($entity_type, array_combine($entity_types, $entity_types));
    $config->set('entity_type', $entity_type);

    $config->save();
    $this->messenger()->addStatus($this->t('The configuration options have been saved.'));
  }

}
