# CONTENTS OF THIS FILE

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


## INTRODUCTION

Limit the number and/or frequency of edits to entities that users can make.

 * For a full description of the module, visit the project page:
   <https://drupal.org/project/edit_limit>

 * To submit bug reports and feature suggestions, or to track changes:
   <https://drupal.org/project/issues/edit_limit>


## REQUIREMENTS

Drupal core only.


## INSTALLATION

Install as you would normally install a contributed Drupal module. Visit:
<https://www.drupal.org/node/1897420> for further information.


## CONFIGURATION

    1. Navigate to Administration > Extend and enable the module.
    2. Navigate to /admin/config/system/edit-limit
    3. Set edit limit settings.
    4. Save configuration.


## MAINTAINERS

 * Andrei Ivnitskii - <https://www.drupal.org/u/ivnish>
