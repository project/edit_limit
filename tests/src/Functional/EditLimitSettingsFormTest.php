<?php

namespace Drupal\Tests\edit_limit\Functional;

use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests for the edit_limit module.
 *
 * @group edit_limit
 */
class EditLimitSettingsFormTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'edit_limit',
    'node',
    'comment',
  ];

  /**
   * The User used for the test.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  private $adminUser;

  /**
   * The User used for the test.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  private $user;

  /**
   * Settings form url.
   */
  protected Url $settingsRoute;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->settingsRoute = Url::fromRoute('edit_limit.form');

    $this->user = $this->DrupalCreateUser();

    $this->adminUser = $this->DrupalCreateUser([
      'administer site configuration',
    ]);

    $this->createContentType(['type' => 'page']);
    $this->createContentType(['type' => 'article']);

    $this->drupalCreateRole([], 'administrator', 'Administrator');
  }

  /**
   * Tests that the settings page can be reached and saved.
   */
  public function testSettingsPage() {
    $this->drupalLogin($this->user);
    $this->drupalGet($this->settingsRoute);
    $this->assertSession()->statusCodeEquals(403);

    $this->drupalLogin($this->adminUser);
    $this->drupalGet($this->settingsRoute);
    $this->assertSession()->statusCodeEquals(200);

    $edit = [
      'ignore[authenticated]' => 'authenticated',
      'ignore[administrator]' => 'administrator',
      'entity_types[node]' => 'node',
      'entity_types[comment]' => 'comment',
      'entity_type[node][limit]' => 2,
      'entity_type[node][unit]' => 'minutes',
      'entity_type[node][bundles][]' => 'article',
    ];
    $expected_values = [
      'ignore' => ['authenticated', 'administrator'],
      'entity_types' => ['comment', 'node'],
      'entity_type' => [
        'comment' => [
          'limit' => 15,
          'unit' => 'minutes',
          'bundles' => [],
        ],
        'node' => [
          'limit' => 2,
          'unit' => 'minutes',
          'bundles' => [
            'article' => 'article',
          ],
        ],
      ],
    ];

    $this->submitForm($edit, 'Save configuration');
    $this->assertSession()->pageTextContains('The configuration options have been saved.');

    foreach ($expected_values as $field => $expected_value) {
      $actual_value = $this->config('edit_limit.settings')->get($field);
      $this->assertEquals($expected_value, $actual_value);
    }
  }

}
